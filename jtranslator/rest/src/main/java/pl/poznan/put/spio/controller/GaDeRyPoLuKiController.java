package pl.poznan.put.spio.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.poznan.put.spio.controller.model.TranslatorResponse;
import pl.poznan.put.spio.translator.GaDeRyPoLuKi;

/*
 * kontroler REST, który udostępnia kilka endpointów za pomocą których można odpytywać translator
 * np.: http://localhost:8080/gaderypoluki/alamakota zwróci nam przetłumaczony tekst opakowany w JSON
 */
@AllArgsConstructor
@RestController
public class GaDeRyPoLuKiController {

    private GaDeRyPoLuKi translator;

    @GetMapping("/gaderypoluki/{origin}")
    public TranslatorResponse translate(@PathVariable final String origin) {
        final TranslatorResponse response = new TranslatorResponse();
        response.setOrigin(origin);
        response.setTranslated(translator.translate(origin));
        return response;
    }

    @GetMapping("/gaderypoluki/{origin}/ignorecase")
    public TranslatorResponse translateIgnore(@PathVariable final String origin) {
        final TranslatorResponse response = new TranslatorResponse();
        response.setOrigin(origin);
        response.setTranslated(translator.translateIgnoreCase(origin));
        return response;
    }

    @GetMapping("/gaderypoluki/{origin}/with/{key}")
    public TranslatorResponse translateWithKey(@PathVariable final String origin, @PathVariable final String key) {
        final TranslatorResponse response = new TranslatorResponse();
        response.setOrigin(origin);
        response.setTranslated(translator.translate(origin, key));
        response.setKey(key);
        return response;
    }

    @GetMapping(path = "/gaderypoluki/{origin}/with/{key}/ignorecase")
    public TranslatorResponse translateWithKeyIgnore(@PathVariable final String origin, @PathVariable final String key) {
        final TranslatorResponse response = new TranslatorResponse();
        response.setOrigin(origin);
        response.setTranslated(translator.translateIgnoreCase(origin, key));
        response.setKey(key);
        return response;
    }

}
